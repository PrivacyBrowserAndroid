• Corrige uma falha ao abrir a função Importar/Exportar.
• Corrige um problema com agente de usuário personalizado.