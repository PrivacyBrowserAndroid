• Implementazione del relativamente nuovo tema Giorno e Notte di Android.
• Passaggio all'utilizzo del relativamente nuovo tema scuro nativo di Webview.
• Salvataggio e ripristino dello stato nel caso in cui Privacy Browser venga riavviato in background dal sistema operativo.
• Utilizzo del "Content-Disposition header" per ottenere i nomi dei file nei download.
• Sistemazione dell'upload dei file verso alcuni siti web.
• Aggiunta di Mojeek ed eliminazione di Qwant e Searx dalla lista dei motori di ricerca.
• Correzione di un baco che talvolta provocava il funzionamento dello swipe per aggiornare la pagina anche se disabilitato.
• Riordino dei menu contestuali.
• Applicazione di intestazioni personalizzate ai collegamenti caricati da Webview.
• Aggiornamento della traduzione Italiana fornita da Francesco Buratti.
• Aggiornamento della traduzione Francese fornita da Kévin L.
• Aggiornamento della traduzione in Russo.
• Aggiornamento della traduzione Spagnola fornita da Jose A. León.
• Aggiornamento della traduzione Tedesco fornita da Bernhard G. Keller.