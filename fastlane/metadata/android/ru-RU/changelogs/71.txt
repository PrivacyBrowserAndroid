• Исправлен сбой, возникавший при перемещении закладки в домашнюю папку.
• Добавлена секретная недокументированная возможность просмотра кода в WebView.
• Исправлена проблема настроек домена, при которой домены иногда создавались со включенными параметрами, вместо параметров по умолчанию.
• User Agent в настройках домена перемещен ниже DOM-хранилища.
• Создан специальный диалог шифрования для контентных URL.
• Исправлен сбой при котором Privacy Browser перезапускался во время отображения диалога об ошибке SSL-сертификата.
• Исправлен сбой при котором Privacy Browser перезапускался во время отображения диалога HTTP-аутентификации.