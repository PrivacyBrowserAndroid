• Suppression de Startpage et passage de Mojeek en page d'accueil et moteur de recherche par défaut.
• Inversion du menu de navigation lorsque la barre des apps est en bas.
• Affichage de la barre d'apps inférieure lorsqu'un nouvel onglet se charge.
• Limitation des filtres d'intention de contenu au texte, aux images et aux fichiers MHT.
• Correction de certains rare crashs.
• Traduction française mise à jour fournie par Kévin L.